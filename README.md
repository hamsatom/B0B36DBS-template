[![pipeline status](https://gitlab.fel.cvut.cz/hamsatom/B0B36DBS-template/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/hamsatom/B0B36DBS-template/commits/master)

**Autor:** Tomáš Hamsa  
**Javadoc:** http://hamsatom.pages.fel.cvut.cz/B0B36DBS-template/
# B0B36DBS Databázové systémy template
Šablona pro semestrální práci na předmět B0B36DBS
![Class diagram](classDiagram.png)
