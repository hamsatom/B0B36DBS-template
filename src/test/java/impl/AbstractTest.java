package impl;

import impl.data.model.AbstractEntity;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;
import org.testng.Assert;
import org.testng.annotations.Test;
import template.ITest;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
public abstract class AbstractTest<T extends AbstractEntity<?>> implements ITest<T> {

  protected abstract Collection<T> list();

  protected abstract Optional<T> save(T entity);

  protected abstract Collection<? extends T> save(Collection<? extends T> entities);

  protected abstract Optional<T> find(Serializable id);

  protected abstract Optional<T> update(T entity);

  protected abstract void delete(Serializable id);

  @Override
  public T uploadEntity() {
    T expectedEntity = createdFirstEntity();
    Optional<T> maybeEntity = save(expectedEntity);
    Assert.assertTrue(maybeEntity.isPresent());
    T actualEntity = maybeEntity.get();
    compareUnsaved(actualEntity, expectedEntity);
    Assert.assertNotNull(actualEntity.getId());
    return actualEntity;
  }

  @Test
  public void testSave() {
    uploadEntity();
  }

  @Test
  public void testSaveAll() {
    T a = createdFirstEntity();
    T b = createdSecondEntity();
    Collection<? extends T> result = save(Arrays.asList(a, b));
    Assert.assertEquals(result.size(), 2);
    Iterator<? extends T> iterator = result.iterator();
    compareUnsaved(iterator.next(), a);
    compareUnsaved(iterator.next(), b);
  }

  @Test
  public void testFind() {
    T savedEntity = uploadEntity();
    Optional<T> maybeFound = find(savedEntity.getId());
    Assert.assertTrue(maybeFound.isPresent());
    Assert.assertEquals(maybeFound.get(), savedEntity);
  }

  @Test
  public void testList() {
    T a = uploadEntity();
    T b = uploadEntity();
    Collection<T> foundEntities = list();
    Assert.assertTrue(foundEntities.contains(a));
    Assert.assertTrue(foundEntities.contains(b));
  }

  @Test
  public void testUpdate() {
    T savedEntity = uploadEntity();
    T changedEntity = changeEntity(savedEntity);
    Optional<T> maybeEntity = update(changedEntity);
    Assert.assertTrue(maybeEntity.isPresent());
    Assert.assertEquals(maybeEntity.get(), changedEntity);
  }

  @Test
  public void testDelete() {
    T entity = uploadEntity();
    Serializable id = entity.getId();
    delete(id);
    Optional<T> foundEntity = find(id);
    Assert.assertFalse(foundEntity.isPresent());
  }
}
