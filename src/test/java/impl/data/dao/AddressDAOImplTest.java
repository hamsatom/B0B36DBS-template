package impl.data.dao;

import impl.data.model.Address;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import template.data.AddressTest;
import template.data.dao.AddressDAO;

public class AddressDAOImplTest extends AbstractDAOTest<Address> implements AddressTest {

  private AddressDAO dao;

  @BeforeMethod
  public void setUp() {
    dao = new AddressDAOImpl();
  }

  @Override
  AddressDAO getDAO() {
    return dao;
  }

  @Test
  @Override
  public void testListInCity() {
    checkListInCity(dao::listInCity);
  }
}