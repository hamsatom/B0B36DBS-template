package impl.data.dao;

import impl.data.model.Person;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import template.data.PersonTest;
import template.data.dao.PersonDAO;

public class PersonDAOImplTest extends AbstractDAOTest<Person> implements PersonTest {

  private PersonDAO dao;

  @BeforeMethod
  public void setUp() {
    dao = new PersonDAOImpl();
  }

  @Override
  PersonDAO getDAO() {
    return dao;
  }

  @Test
  @Override
  public void testListAfterDate() {
    checkListAfterDate(dao::listBornAfterDate);
  }
}