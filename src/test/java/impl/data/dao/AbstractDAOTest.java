package impl.data.dao;

import impl.AbstractTest;
import impl.data.model.AbstractEntity;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import template.data.dao.DAO;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
abstract class AbstractDAOTest<T extends AbstractEntity<?>> extends AbstractTest<T> {

  abstract DAO<T> getDAO();

  @Override
  protected List<T> list() {
    return getDAO().list();
  }

  @Override
  protected Optional<T> save(T entity) {
    return getDAO().save(entity);
  }

  @Override
  protected Collection<? extends T> save(Collection<? extends T> entities) {
    return getDAO().save(entities);
  }

  @Override
  protected Optional<T> find(Serializable id) {
    return getDAO().find(id);
  }

  @Override
  protected Optional<T> update(T entity) {
    return getDAO().update(entity);
  }

  @Override
  protected void delete(Serializable id) {
    getDAO().delete(id);
  }
}
