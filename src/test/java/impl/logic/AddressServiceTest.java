package impl.logic;

import impl.data.model.Address;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import template.data.AddressTest;
import template.logic.AddressService;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public class AddressServiceTest extends AbstractServiceTest<Address> implements AddressTest {

  private AddressService service;

  @BeforeMethod
  public void setUp() {
    service = new AddressServiceImpl();
  }

  @Override
  AddressService getService() {
    return service;
  }

  @Test
  @Override
  public void testListInCity() {
    checkListInCity(service::listInCity);
  }
}
