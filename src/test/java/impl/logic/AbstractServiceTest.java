package impl.logic;

import impl.AbstractTest;
import impl.data.model.AbstractEntity;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.testng.Assert;
import org.testng.annotations.Test;
import template.logic.Service;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
abstract class AbstractServiceTest<T extends AbstractEntity<?>> extends AbstractTest<T> {

  abstract Service<T> getService();

  @Override
  protected List<T> list() {
    return getService().list();
  }

  @Override
  protected Optional<T> save(T entity) {
    return getService().save(entity);
  }

  @Override
  protected Collection<? extends T> save(Collection<? extends T> entities) {
    return getService().save(entities);
  }

  @Override
  protected Optional<T> find(Serializable id) {
    return getService().find(id);
  }

  @Override
  protected Optional<T> update(T entity) {
    return getService().update(entity);
  }

  @Override
  protected void delete(Serializable id) {
    getService().delete(id);
  }

  @Test
  public void testExists() {
    Service<T> service = getService();
    T entity = uploadEntity();
    Serializable id = entity.getId();
    Assert.assertTrue(service.exists(id));
    service.delete(id);
    Assert.assertFalse(service.exists(id));
  }
}
