package impl.logic;

import impl.data.model.Person;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import template.data.PersonTest;
import template.logic.PersonService;

public class PersonServiceImplTest extends AbstractServiceTest<Person> implements PersonTest {

  private PersonService service;

  @BeforeMethod
  public void setUp() {
    service = new PersonServiceImpl();
  }

  @Override
  PersonService getService() {
    return service;
  }

  @Test
  @Override
  public void testListAfterDate() {
    checkListAfterDate(service::listBornAfterDate);
  }
}