package template.data;

import impl.data.model.Address;
import java.util.Collection;
import java.util.function.Function;
import org.testng.Assert;
import template.ITest;

public interface AddressTest extends ITest<Address> {

  void testListInCity();

  default void checkListInCity(Function<String, Collection<Address>> provider) {
    Address address = uploadEntity();
    String city = address.getCity();

    Collection<Address> addresses = provider.apply(city);
    Assert.assertTrue(addresses.contains(address));

    addresses = provider.apply('n' + city);
    Assert.assertFalse(addresses.contains(address));
  }

  @Override
  default void compareUnsaved(Address actual, Address expected) {
    Assert.assertEquals(actual.getCity(), expected.getCity());
    Assert.assertEquals(actual.getPeople(), expected.getPeople());
    Assert.assertEquals(actual.getPostalCode(), expected.getPostalCode());
    Assert.assertEquals(actual.getStreet(), expected.getStreet());
  }

  @Override
  default Address createdFirstEntity() {
    return new Address()
        .setCity("Praha")
        .setPostalCode(123)
        .setStreet("Technická");
  }

  @Override
  default Address createdSecondEntity() {
    return new Address()
        .setCity("Ostrava pyčo")
        .setPostalCode(456)
        .setStreet("Stodolní");
  }

  @Override
  default Address changeEntity(Address entity) {
    return entity.setCity("Brno")
        .setStreet("Vesnická")
        .setPostalCode(789);
  }
}
