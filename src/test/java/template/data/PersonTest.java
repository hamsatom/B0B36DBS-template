package template.data;

import impl.data.model.Person;
import java.sql.Date;
import java.util.Collection;
import java.util.function.Function;
import org.testng.Assert;
import template.ITest;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public interface PersonTest extends ITest<Person> {

  void testListAfterDate();

  default void checkListAfterDate(Function<Date, Collection<Person>> provider) {
    Person person = uploadEntity();
    long time = person.getBirthday().getTime();

    Date dateBefore = new Date(time - 1);
    Collection<Person> people = provider.apply(dateBefore);
    Assert.assertTrue(people.stream().allMatch(p -> p.getBirthday().after(dateBefore)));
  }

  @Override
  default void compareUnsaved(Person actual, Person expected) {
    Assert.assertEquals(actual.getBirthday(), expected.getBirthday());
    Assert.assertEquals(actual.getFirstName(), expected.getFirstName());
    Assert.assertEquals(actual.getLastName(), expected.getLastName());
  }

  @Override
  default Person createdFirstEntity() {
    return new Person()
        .setFirstName("Šimon")
        .setLastName("Maňour")
        .setBirthday(new Date(System.currentTimeMillis()));
  }

  @Override
  default Person createdSecondEntity() {
    return new Person()
        .setFirstName("Tomáš")
        .setLastName("Nereda")
        .setBirthday(new Date(System.currentTimeMillis()));
  }

  @Override
  default Person changeEntity(Person entity) {
    return entity.setFirstName("Jan")
        .setLastName("Skála")
        .setBirthday(new Date(1L));
  }
}
