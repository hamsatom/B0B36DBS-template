package template;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
public interface ITest<T> {

  T uploadEntity();

  void compareUnsaved(T actual, T expected);

  T createdFirstEntity();

  T createdSecondEntity();

  T changeEntity(T entity);
}
