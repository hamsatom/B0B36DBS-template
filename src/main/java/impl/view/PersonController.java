package impl.view;

import impl.data.model.Person;
import impl.logic.PersonServiceImpl;
import java.net.URL;
import java.sql.Date;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import template.logic.PersonService;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
public class PersonController implements Initializable {

  private static final Logger LOG = Logger.getLogger(PersonController.class.getName());
  private static final PersonService SERVICE = new PersonServiceImpl();

  @FXML
  public Text people;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    LOG.info(() -> "Initializing " + PersonController.class.getSimpleName());

    Person person = new Person()
        .setFirstName("Šimon")
        .setLastName("Maňour")
        .setBirthday(new Date(System.currentTimeMillis()));
    SERVICE.save(person);

    String dbContent = SERVICE.list()
        .stream()
        .sorted((a, b) -> b.getBirthday().compareTo(a.getBirthday()))
        .map(Objects::toString)
        .collect(Collectors.joining(System.lineSeparator()));
    people.setText("People in DB: " + dbContent);
  }
}
