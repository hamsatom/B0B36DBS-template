package impl.view;

import impl.data.model.Address;
import impl.logic.AddressServiceImpl;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Text;
import template.logic.AddressService;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public class AddressController implements Initializable {

  private static final Logger LOG = Logger.getLogger(AddressController.class.getName());
  private static final AddressService SERVICE = new AddressServiceImpl();

  @FXML
  public Text addresses;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    LOG.info(() -> "Initializing " + AddressController.class.getSimpleName());

    Address address = new Address()
        .setCity("Brno")
        .setPostalCode(123)
        .setStreet("Stodolní");
    SERVICE.save(address);

    String dbContent = SERVICE.list()
        .stream()
        .map(Objects::toString)
        .collect(Collectors.joining(System.lineSeparator()));
    addresses.setText("Addresses in DB: " + dbContent);
  }
}
