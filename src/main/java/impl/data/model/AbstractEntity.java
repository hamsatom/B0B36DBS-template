package impl.data.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntity<T extends Serializable> implements Serializable {

  private static final long serialVersionUID = -7833250002613167643L;

  @Id
  @GeneratedValue
  private T id;

  public T getId() {
    return id;
  }

  public AbstractEntity setId(T id) {
    this.id = id;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof AbstractEntity)) {
      return false;
    }
    AbstractEntity<?> that = (AbstractEntity<?>) o;
    return Objects.equals(id, that.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return "AbstractEntity{id=" + id + '}';
  }
}