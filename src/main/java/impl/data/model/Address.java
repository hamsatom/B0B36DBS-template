package impl.data.model;

import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
@Entity
public class Address extends AbstractEntity<String> {

  private static final long serialVersionUID = -5899470647842464878L;

  @Column(nullable = false)
  @Basic(optional = false)
  private String city;

  @Column(nullable = false)
  @Basic(optional = false)
  private long postalCode;

  @Column(nullable = false)
  @Basic(optional = false)
  private String street;

  @ManyToMany(mappedBy = "addresses", cascade = {CascadeType.MERGE, CascadeType.PERSIST,
      CascadeType.REFRESH})
  private List<Person> people;

  public String getCity() {
    return city;
  }

  public Address setCity(String city) {
    this.city = city;
    return this;
  }

  public long getPostalCode() {
    return postalCode;
  }

  public Address setPostalCode(long postalCode) {
    this.postalCode = postalCode;
    return this;
  }

  public String getStreet() {
    return street;
  }

  public Address setStreet(String street) {
    this.street = street;
    return this;
  }

  public List<Person> getPeople() {
    return people;
  }

  public Address setPeople(List<Person> people) {
    this.people = people;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Address)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Address address = (Address) o;
    return postalCode == address.postalCode &&
        Objects.equals(city, address.city) &&
        Objects.equals(street, address.street);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), city, postalCode, street);
  }

  @Override
  public String toString() {
    return "Address{" +
        "city='" + city + '\'' +
        ", postalCode=" + postalCode +
        ", street='" + street + '\'' +
        "} " + super.toString();
  }
}
