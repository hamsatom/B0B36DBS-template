package impl.data.model;

import java.sql.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "people")
@NamedQuery(name = Person.SELECT_AFTER_DATE, query = "SELECT p FROM Person p WHERE p.birthday > :date")
public class Person extends AbstractEntity<Integer> {

  public static final String SELECT_AFTER_DATE = "SELECT_AFTER_DATE";

  private static final long serialVersionUID = -7993868297085230282L;

  @Column(name = "first_name", nullable = false)
  @Basic(optional = false)
  private String firstName;

  @Column(name = "last_name", nullable = false)
  @Basic(optional = false)
  private String lastName;

  @Column(nullable = false)
  @Basic(optional = false)
  private Date birthday;

  @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
  @JoinTable(name = "people_address")
  private List<Address> addresses;

  public String getFirstName() {
    return firstName;
  }

  public Person setFirstName(String firstName) {
    this.firstName = firstName;
    return this;
  }

  public String getLastName() {
    return lastName;
  }

  public Person setLastName(String lastName) {
    this.lastName = lastName;
    return this;
  }

  public Date getBirthday() {
    return birthday;
  }

  public Person setBirthday(Date birthday) {
    this.birthday = birthday;
    return this;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  public Person setAddresses(List<Address> addresses) {
    this.addresses = addresses;
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Person)) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    Person person = (Person) o;
    return Objects.equals(firstName, person.firstName) &&
        Objects.equals(lastName, person.lastName) &&
        Objects.equals(birthday, person.birthday);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), firstName, lastName, birthday);
  }

  @Override
  public String toString() {
    return "Person{" +
        "firstName='" + firstName + '\'' +
        ", lastName='" + lastName + '\'' +
        ", birthday=" + birthday +
        "} " + super.toString();
  }
}
