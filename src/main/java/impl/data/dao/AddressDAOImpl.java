package impl.data.dao;

import impl.data.model.Address;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import template.data.dao.AddressDAO;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public class AddressDAOImpl extends AbstractDAO<Address> implements AddressDAO {

  private static final Logger LOG = Logger.getLogger(AddressDAOImpl.class.getName());

  @Nonnull
  @Override
  public List<Address> listInCity(@Nonnull String city) {
    LOG.info(() -> "Listing in city " + city);
    return doSafeTransaction(em -> {
      CriteriaBuilder builder = em.getCriteriaBuilder();
      CriteriaQuery<Address> query = builder.createQuery(Address.class);
      Root<Address> from = query.from(Address.class);

      query.select(from);
      query.where(builder.equal(from.get("city"), city));

      return em.createQuery(query).getResultList();
    });
  }
}
