package impl.data.dao;

import impl.data.model.Person;
import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import javax.persistence.TypedQuery;
import template.data.dao.PersonDAO;

/**
 * @author Tomáš Hamsa on 17.05.2018.
 */
public class PersonDAOImpl extends AbstractDAO<Person> implements PersonDAO {

  private static final Logger LOG = Logger.getLogger(PersonDAOImpl.class.getName());

  @Nonnull
  @Override
  public List<Person> listBornAfterDate(@Nonnull Date date) {
    LOG.info(() -> "Listing after date " + date);
    return doSafeTransaction(em -> {
      TypedQuery<Person> query = em.createNamedQuery(Person.SELECT_AFTER_DATE, Person.class);
      query.setParameter("date", date);
      return query.getResultList();
    });
  }
}
