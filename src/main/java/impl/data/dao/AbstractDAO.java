package impl.data.dao;


import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Logger;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import template.data.dao.DAO;

/**
 * @author Tomáš Hamsa on 16.05.2018.
 */
abstract class AbstractDAO<T extends Serializable> implements DAO<T> {

  private static final Logger LOG = Logger.getLogger(AbstractDAO.class.getName());
  private static final EntityManagerFactory MANAGER_FACTORY = Persistence
      .createEntityManagerFactory("persistenceUnit");

  private final Class<T> type;

  @SuppressWarnings("unchecked")
  AbstractDAO() {
    Type t = getClass().getGenericSuperclass();
    ParameterizedType pt = (ParameterizedType) t;
    type = (Class) pt.getActualTypeArguments()[0];
  }

  static void doSafeTransactionWithoutResult(@Nonnull Consumer<? super EntityManager> function) {
  	doSafeTransaction(em -> {
		function.accept(em);
		return null;
	});
  }

  static <U> U doSafeTransaction(@Nonnull Function<? super EntityManager, U> function) {
    EntityManager em = MANAGER_FACTORY.createEntityManager();
    EntityTransaction transaction = em.getTransaction();
    try {
      transaction.begin();

      U result = function.apply(em);

      transaction.commit();
      return result;

    } finally {
      if (transaction.isActive()) {
        transaction.rollback();
      }
      em.close();
    }
  }

  @Nonnull
  @Override
  public List<T> list() {
    LOG.info(() -> "Listing " + type);
    return doSafeTransaction(em -> {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<T> cq = cb.createQuery(type);
      Root<T> rootEntry = cq.from(type);
      CriteriaQuery<T> all = cq.select(rootEntry);
      TypedQuery<T> allQuery = em.createQuery(all);
      return allQuery.getResultList();
    });
  }

  @Nonnull
  @Override
  public Optional<T> save(@Nonnull T entity) {
    LOG.info(() -> "Persisting: " + entity);
    doSafeTransactionWithoutResult(em -> em.persist(entity));
    return Optional.ofNullable(entity);
  }

  @Nonnull
  @Override
  public Collection<? extends T> save(@Nonnull Collection<? extends T> entities) {
    LOG.info(() -> "Persisting: " + entities);
    doSafeTransactionWithoutResult(em -> entities.forEach(em::persist));
    return entities;
  }

  @Nonnull
  @Override
  public Optional<T> find(@Nonnull Serializable id) {
    LOG.info(() -> "Finding " + type + " with id " + id);
    return doSafeTransaction(em -> {
      T result = em.find(type, id);
      return Optional.ofNullable(result);
    });
  }

  @Nonnull
  @Override
  public Optional<T> update(@Nonnull T entity) {
    LOG.info(() -> "Updating: " + entity);
    return doSafeTransaction(em -> {
      T result = em.merge(entity);
      return Optional.ofNullable(result);
    });
  }

  @Override
  public void delete(@Nonnull Serializable id) {
    LOG.info(() -> "Deleting " + type + " with id " + id);
    doSafeTransactionWithoutResult(em -> {
      T reference = em.getReference(type, id);
      em.remove(reference);
    });
  }
}
