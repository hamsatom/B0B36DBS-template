package impl.logic;

import impl.data.dao.PersonDAOImpl;
import impl.data.model.Person;
import java.sql.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import template.data.dao.PersonDAO;
import template.logic.PersonService;

/**
 * @author Tomáš Hamsa on 16.05.2018.
 */
public class PersonServiceImpl extends AbstractService<Person> implements PersonService {

  private static final Logger LOG = Logger.getLogger(PersonServiceImpl.class.getName());
  private final PersonDAO dao = new PersonDAOImpl();

  @Nonnull
  @Override
  PersonDAO getDAO() {
    return dao;
  }

  @Nonnull
  @Override
  public List<Person> listBornAfterDate(@Nonnull Date date) {
    LOG.info(() -> "Listing after date " + date);
    return dao.listBornAfterDate(date);
  }
}
