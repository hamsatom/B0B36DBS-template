package impl.logic;

import impl.data.dao.AddressDAOImpl;
import impl.data.model.Address;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import template.data.dao.AddressDAO;
import template.logic.AddressService;

/**
 * @author Tomáš Hamsa on 15.07.2018.
 */
public class AddressServiceImpl extends AbstractService<Address> implements AddressService {

  private static final Logger LOG = Logger.getLogger(AddressServiceImpl.class.getName());
  private final AddressDAO dao = new AddressDAOImpl();

  @Nonnull
  @Override
  AddressDAO getDAO() {
    return dao;
  }

  @Nonnull
  @Override
  public List<Address> listInCity(@Nonnull String city) {
    LOG.info(() -> "Listing in city " + city);
    return dao.listInCity(city);
  }
}
