package impl.logic;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import javax.annotation.Nonnull;
import template.data.dao.DAO;
import template.logic.Service;

/**
 * @author Tomáš Hamsa on 17.05.2018.
 */
abstract class AbstractService<T> implements Service<T> {

  private static final Logger LOG = Logger.getLogger(AbstractService.class.getName());

  @Nonnull
  abstract DAO<T> getDAO();

  @Nonnull
  @Override
  public List<T> list() {
    LOG.info(() -> "Listing");
    List<T> result = getDAO().list();
    result.forEach(this::postLoad);
    return result;
  }

  @Nonnull
  @Override
  public Optional<T> save(@Nonnull T entity) {
    LOG.info(() -> "Persisting: " + entity);
    prePersist(entity);
    return getDAO().save(entity);
  }

  @Nonnull
  @Override
  public Collection<? extends T> save(@Nonnull Collection<? extends T> entities) {
    LOG.info(() -> "Persisting: " + entities);
    entities.forEach(this::prePersist);
    return getDAO().save(entities);
  }

  @Nonnull
  @Override
  public Optional<T> update(@Nonnull T entity) {
    LOG.info(() -> "Updating: " + entity);
    preUpdate(entity);
    Optional<T> result = getDAO().update(entity);
    postUpdate();
    return result;
  }

  @Nonnull
  @Override
  public Optional<T> find(@Nonnull Serializable id) {
    LOG.info(() -> "Finding with id " + id);
    Optional<T> result = getDAO().find(id);
    result.ifPresent(this::postLoad);
    return result;
  }

  @Override
  public boolean exists(@Nonnull Serializable id) {
    LOG.info(() -> "Exists with id " + id);
    return getDAO().find(id).isPresent();
  }

  @Override
  public void delete(@Nonnull Serializable id) {
    LOG.info(() -> "Deleting with id " + id);
    getDAO().delete(id);
  }

  // The following methods are hooks intended to be overridden by subclasses, so that the main CRUD methods do not have to be modified

  void prePersist(@Nonnull T entity) {
    /* Do nothing, intended for overriding */
  }

  void preUpdate(@Nonnull T entity) {
    /* Do nothing, intended for overriding */
  }

  void postUpdate() {
    /* Do nothing, intended for overriding */
  }

  void postLoad(@Nonnull T entity) {
    /* Do nothing, intended for overriding */
  }
}
