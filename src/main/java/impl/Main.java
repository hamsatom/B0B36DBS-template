package impl;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * @author Tomáš Hamsa on 14.07.2018.
 */
public class Main extends Application {

  private static final String MAIN_WINDOW_FORM = "/gui-form/person.fxml";

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    primaryStage.setOnCloseRequest(e -> {
      Platform.exit();
      System.exit(0);
    });
    Parent root = FXMLLoader.load(getClass().getResource(MAIN_WINDOW_FORM));
    primaryStage.setScene(new Scene(root));

    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    primaryStage.setMaxHeight(primaryScreenBounds.getHeight());
    primaryStage.setMaxWidth(primaryScreenBounds.getWidth());

    primaryStage.show();
  }
}
