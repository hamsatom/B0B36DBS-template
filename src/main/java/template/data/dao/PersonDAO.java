package template.data.dao;

import impl.data.model.Person;
import java.sql.Date;
import java.util.List;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 16.05.2018.
 */
public interface PersonDAO extends DAO<Person> {

  @Nonnull
  List<Person> listBornAfterDate(@Nonnull Date date);
}
