package template.data.dao;

import impl.data.model.Address;
import java.util.List;
import javax.annotation.Nonnull;

public interface AddressDAO extends DAO<Address> {

  @Nonnull
  List<Address> listInCity(@Nonnull String city);
}
