package template.data.dao;


import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa
 */
public interface DAO<T> {

  @Nonnull
  List<T> list();

  @Nonnull
  Optional<T> save(@Nonnull T entity);

  @Nonnull
  Collection<? extends T> save(@Nonnull Collection<? extends T> entities);

  @Nonnull
  Optional<T> find(@Nonnull Serializable id);

  @Nonnull
  Optional<T> update(@Nonnull T entity);

  void delete(@Nonnull Serializable id);
}
