package template.logic;

import impl.data.model.Address;
import java.util.List;
import javax.annotation.Nonnull;

public interface AddressService extends Service<Address> {

  @Nonnull
  List<Address> listInCity(@Nonnull String city);
}
