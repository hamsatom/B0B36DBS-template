package template.logic;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.annotation.Nonnull;

public interface Service<T> {

  @Nonnull
  List<T> list();

  @Nonnull
  Optional<T> save(@Nonnull T entity);

  @Nonnull
  Collection<? extends T> save(@Nonnull Collection<? extends T> entities);

  @Nonnull
  Optional<T> find(@Nonnull Serializable id);

  @Nonnull
  Optional<T> update(@Nonnull T entity);

  /**
   * Checks whether an instance with the specified id exists.
   *
   * @param id Instance identifier
   * @return Whether a matching instance exists
   */
  boolean exists(@Nonnull Serializable id);

  void delete(@Nonnull Serializable id);
}
