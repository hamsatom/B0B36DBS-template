package template.logic;

import impl.data.model.Person;
import java.sql.Date;
import java.util.List;
import javax.annotation.Nonnull;

public interface PersonService extends Service<Person> {

  @Nonnull
  List<Person> listBornAfterDate(@Nonnull Date date);
}
